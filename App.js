import React from "react";
import { StyleSheet, SafeAreaView, StatusBar } from "react-native";
import WelcomeScreen from "./app/screens/WelcomeScreen";
import ViewImageScreen from "./app/screens/ViewImageScreen";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Meme from "./app/screens/Meme";
import MemeGenerated from "./app/screens/MemeGenerated";

export default function App() {
  const Stack = createStackNavigator();

  return (
    <SafeAreaView style={styles.container}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="WelcomeScreen" component={WelcomeScreen} />
          <Stack.Screen name="Meme" component={Meme} />
          <Stack.Screen name="MemeGenerated" component={MemeGenerated} />
          <Stack.Screen name="ViewImageScreen" component={ViewImageScreen} />
        </Stack.Navigator>
      </NavigationContainer>
      <StatusBar style="auto" />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
