import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  Text,
} from "react-native";

export default function Meme() {
  const [memes, setMemes] = useState([]);
  const [memeIndex, setMemeIndex] = useState(0);
  const [captions, setCaptions] = useState([]);


  const updateCaption = (e, index) => {
    const text = e.target.value || "";
    setCaptions(
      captions.map((c, i) => {
        if (index === i) {
          return text;
        } else {
          return c;
        }
      })
    );
  };

  const generateMeme = () => {
    const currentMeme = memes[memeIndex];
    const formData = new FormData();

    formData.append("username", "NathanFroese");
    formData.append("password", "abc123");
    formData.append("template_id", currentMeme.id);
    captions.forEach((c, index) => formData.append(`boxes[${index}][text]`, c));
console.log(formData);

    fetch("https://api.imgflip.com/caption_image", {
      method: "POST",
      body: formData,
    }).then((res) => {
      res.json().then((res) => {
        console.log(res);
        const url = res.data.url;
        console.log(url);
      });
    });
  };
  const shuffleMemes = (array) => {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * i);
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  };

  useEffect(() => {
    fetch("https://api.imgflip.com/get_memes").then((res) => {
      res.json().then((res) => {
        const _memes = res.data.memes;
        shuffleMemes(_memes);
        setMemes(_memes);
      });
    });
  }, []);

  useEffect(() => {
    if (memes.length) {
      setCaptions(Array(memes[memeIndex].box_count).fill(""));
    }
  }, [memeIndex, memes]);

  return memes.length ? (
    <SafeAreaView style={styles.container}>
      <View>
        <TouchableOpacity
          color="#f194ff"
          onPress={generateMeme}
          style={styles.generate}
        >
          <Text>🐶 Generate</Text>
        </TouchableOpacity>
        <TouchableOpacity
          color="#f194ff"
          onPress={() => setMemeIndex(memeIndex + 1)}
          style={styles.skip}
        >
          <Text>Skip</Text>
        </TouchableOpacity>
        {captions.map((c, index) => (
          <TextInput
            style={styles.textinputbox}
            onChange={(e) => updateCaption(e, index)}
            key={index}
          />
        ))}
        <Image
          alt="meme"
          source={{ url: memes[memeIndex].url }}
          style={{
            flex: 1,
            resizeMode: "contain",
            height: 150,
            width: 250,
          }}
        />
      </View>
    </SafeAreaView>
  ) : (
    <></>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    marginHorizontal: 16,
  },
  skip: {
    marginVertical: 8,
    backgroundColor: "#FFA500",
    alignItems: "center",
  },
  generate: {
    marginVertical: 8,
    backgroundColor: "#32CD32",
    alignItems: "center",
  },
  textinputbox: {
    height: 40,
    borderColor: "gray",
    borderWidth: 2,
  },
});
