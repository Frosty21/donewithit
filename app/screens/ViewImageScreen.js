import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Meme from "./Meme";

export default function ViewImageScreen() {
  const loadNewImage = () => {
    fetch(
      "https://ronreiter-meme-generator.p.rapidapi.com/meme?font=Impact&font_size=50&meme=Condescending-Wonka&top=Top%20text&bottom=Bottom%20text",
      {
        method: "GET",
        headers: {
          "x-rapidapi-host": "ronreiter-meme-generator.p.rapidapi.com",
          "x-rapidapi-key":
            "1fee9d58c6msh63add06cbb1b99dp1c7936jsncb3cf90b09d8",
        },
      }
    );
  };
  return (
    <View>
      <TouchableOpacity onPress={loadNewImage()}>
        <Meme />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },
});
